﻿using System;
using System.IO;
using System.Collections.Generic;

namespace c_code
{

    public enum Direction
    {
        Left,  // x-1
        Right, // x+1
        Down,  // y-1
        Up     // y+1
    }
    public interface IMaze
    {
        LocationDetails location { get; set; }

        bool TryMoveMouse(Direction direction);

        bool FoundCheese();
    }
    public class Maze : IMaze
    {
        private const int maxX = 100;
        private const int maxY = 100;

        public LocationDetails location { get; set; }
        public bool TryMoveMouse(Direction direction)
        {
            var newMouseLocation = new Location(location.Mouse.X, location.Mouse.Y);
            switch (direction)
            {
                case Direction.Down:
                    {
                        newMouseLocation.Y--;
                        break;
                    }
                case Direction.Up:
                    {
                        newMouseLocation.Y++;
                        break;
                    }
                case Direction.Left:
                    {
                        newMouseLocation.X--;
                        break;
                    }
                case Direction.Right:
                    {
                        newMouseLocation.X++;
                        break;
                    }
            }

            if (newMouseLocation.X >= maxX || newMouseLocation.Y >= maxY || newMouseLocation.X < 0 || newMouseLocation.Y < 0)
                return false;
            if (location.Walls.Exists(x => x.Equals(newMouseLocation)))
                return false;

            location.Mouse = newMouseLocation;
            return true;
        }
        public bool FoundCheese()
        {
            return location.Mouse.Equals(location.Cheese);
        }
    }
    public class Location
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Location(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override bool Equals(Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Location l = (Location)obj;
            return (l.X == this.X) && (l.Y == this.Y);
        }
        public override int GetHashCode()
        {
            return this.X ^ this.Y;
        }
    }
    public class LocationDetails
    {
        public Location Mouse;
        public Location Cheese;
        public List<Location> Walls;
    }
    class Program
    {
        public static bool HandleDirection(IMaze maze, Direction direction, Direction returnDirection, List<Location> alreadyChecked)
        {
            bool cheeseFound = false;
            if (maze.TryMoveMouse(direction))
            {
                if (alreadyChecked.Exists(x => x.Equals(maze.location.Mouse)))
                {
                    maze.TryMoveMouse(returnDirection);
                    return false;
                }
                alreadyChecked.Add(new Location(maze.location.Mouse.X, maze.location.Mouse.Y));
                if (maze.FoundCheese())
                {
                    Console.WriteLine("Success!");
                    Console.WriteLine("maze Location found x:{0},y:{1}:", maze.location.Mouse.X, maze.location.Mouse.Y);
                    return true;
                }
                else
                    cheeseFound = FindCheese(maze, returnDirection, alreadyChecked);

                if (cheeseFound == true)
                    return true;
                maze.TryMoveMouse(returnDirection);
            }
            return cheeseFound;
        }
        public static bool FindCheese(IMaze maze, Direction? notDirection, List<Location> alreadyChecked)
        {
            bool cheeseFound = false;
            if (notDirection == null || notDirection != Direction.Down)
            {
                cheeseFound = HandleDirection(maze, Direction.Down, Direction.Up, alreadyChecked);
                if (cheeseFound == true)
                    return true;
            }
            if (notDirection == null || notDirection != Direction.Up)
            {
                cheeseFound = HandleDirection(maze, Direction.Up, Direction.Down, alreadyChecked);
                if (cheeseFound == true)
                    return true;
            }
            if (notDirection == null || notDirection != Direction.Right)
            {
                cheeseFound = HandleDirection(maze, Direction.Right, Direction.Left, alreadyChecked);
                if (cheeseFound == true)
                    return true;
            }
            if (notDirection == null || notDirection != Direction.Left)
            {
                cheeseFound = HandleDirection(maze, Direction.Left, Direction.Right, alreadyChecked);
                if (cheeseFound == true)
                    return true;
            }


            return false;
        }

        public static Location RandomLocation()
        {
            Random random = new Random();
            int x = random.Next(0, 100);
            int y = random.Next(0, 100);
            return new Location(x, y);
        }

        static void Main(string[] args)
        {
             List<Location> alreadyChecked = new List<Location>();
             Maze maze = new Maze();
             maze.location = new LocationDetails();
             maze.location.Cheese = RandomLocation();
             Console.WriteLine("maze Cheese location x:{0},y:{1}:", maze.location.Cheese.X, maze.location.Cheese.Y);
             do
             {
                 maze.location.Mouse = RandomLocation();
             } while (maze.location.Cheese.Equals(maze.location.Mouse));

             Random random = new Random();
             int numerOfWall = 100;
             maze.location.Walls = new List<Location>();
             Location wall;
             for (int i = 0; i < numerOfWall; i++)
             {
                 do
                 {
                     wall = RandomLocation();
                 } while (wall.Equals(maze.location.Mouse) || wall.Equals(maze.location.Cheese));
                 maze.location.Walls.Add(wall);
             }
             FindCheese(maze, null, alreadyChecked);
             Console.WriteLine("Finished!");
           /* Maze maze = new Maze();
            List<Location> alreadyChecked = new List<Location>();
            maze.location = new LocationDetails();
            maze.location.Walls = new List<Location>();
            maze.location.Mouse = new Location(0, 0);
            maze.location.Walls.Add(new Location(4, 0));
            maze.location.Walls.Add(new Location(4, 1));
            maze.location.Walls.Add(new Location(4, 2));

            maze.location.Walls.Add(new Location(0, 1));
            maze.location.Walls.Add(new Location(1, 1));
            maze.location.Walls.Add(new Location(2, 1));

            maze.location.Walls.Add(new Location(0, 2));
            maze.location.Walls.Add(new Location(1, 2));
            maze.location.Walls.Add(new Location(2, 2));

            maze.location.Walls.Add(new Location(0, 3));
            maze.location.Walls.Add(new Location(1, 3));
            maze.location.Walls.Add(new Location(2, 3));


            maze.location.Walls.Add(new Location(0, 4));
            maze.location.Walls.Add(new Location(1, 4));
            maze.location.Walls.Add(new Location(2, 4));
            maze.location.Walls.Add(new Location(3, 4));


            maze.location.Cheese = new Location(4, 4);

            FindCheese(maze, null, alreadyChecked);
            Console.WriteLine("Finished! 2");*/
        }
    }
}
